/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_DENSE_CPU_GEMV_H
#define TRINNITY_DENSE_CPU_GEMV_H

// This is a bunch of preprocessor nonsense that
// figures out what GEMV implementation to use.
// We use a wrapper function gemv() as the single point of entry.

#if defined(TRINNITY_USE_MKL_GEMV)
#include <mkl.h>

#elif defined(TRINNITY_USE_CBLAS_GEMV)
extern "C" {
  #include <cblas.h>
}
#endif

#include <triNNity/config.h>
#include <triNNity/dense/cpu/config.h>
#include <triNNity/dense/cpu/primitive.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

namespace triNNity {

namespace dense {

namespace cpu {

namespace internal {

/*
 *  All of our GEMV functions require the dimensions passed to be the dimensions *as declared in the calling subprogram*.
 *  We never do automatic transposition of matrices.
 */

#if defined(TRINNITY_USE_MKL_GEMV) || defined(TRINNITY_USE_CBLAS_GEMV)

template <gemv_transpose_t transp>
static TRINNITY_INLINE void blas_gemv_wrap (signed mat_a_w, signed mat_a_h, const double* __restrict__ a, const double* __restrict__ b, gemv_accumulate_t accum, double* __restrict__ c) {

  double d_accum, d_alpha;
  d_alpha = 1.0;

  switch (accum) {
    case GEMV_ACCUMULATE: {
      d_accum = 1.0;
    } break;

    case GEMV_NO_ACCUMULATE: {
      d_accum = 0.0;
    } break;
  }

  switch (transp) {
    case GEMV_A_B: {
      cblas_dgemv(CblasRowMajor, CblasNoTrans, mat_a_h, mat_a_w, d_alpha, a, mat_a_w, b, 1, d_accum, c, 1);
    } break;

    case GEMV_AT_B: {
      cblas_dgemv(CblasRowMajor, CblasTrans, mat_a_h, mat_a_w, d_alpha, a, mat_a_w, b, 1, d_accum, c, 1);
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

template <gemv_transpose_t transp>
static TRINNITY_INLINE void blas_gemv_wrap (signed mat_a_w, signed mat_a_h, const float* __restrict__ a, const float* __restrict__ b, gemv_accumulate_t accum, float* __restrict__ c) {
  float d_accum, d_alpha;
  d_alpha = 1.0f;

  switch (accum) {
    case GEMV_ACCUMULATE: {
      d_accum = 1.0f;
    } break;

    case GEMV_NO_ACCUMULATE: {
      d_accum = 0.0f;
    } break;
  }

  switch (transp) {
    case GEMV_A_B: {
      cblas_sgemv(CblasRowMajor, CblasNoTrans, mat_a_h, mat_a_w, d_alpha, a, mat_a_w, b, 1, d_accum, c, 1);
    } break;

    case GEMV_AT_B: {
      cblas_sgemv(CblasRowMajor, CblasTrans, mat_a_h, mat_a_w, d_alpha, a, mat_a_w, b, 1, d_accum, c, 1);
    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}
#endif

}

/**
 * This function is a wrapper for the GEMV routine for matrix-vector multiplication.
 *
 * \tparam variant
 *
 * \parblock
 * For platforms where a high-performance BLAS library is available, this parameter can be set to GEMV_BLAS to
 * perform the matrix multiplication using the BLAS library. In this case, define one of
 * \c TRINNITY_USE_CBLAS_GEMV , \c TRINNITY_USE_MKL_GEMV , or \c TRINNITY_USE_CUBLAS_GEMV to select the implementation.
 * You will need to ensure that the compiler and linker options are set so that the BLAS library is visible for compilation and linking.
 * \endparblock
 * \tparam accum
 * \parblock
 * Whether or not the GEMV routine should accumulate into the output.
 * \endparblock
 * \tparam transp
 * \parblock
 * Whether the matrix passed to GEMV is transposed.
 * \endparblock
 */

template <typename T, typename KD, typename C,
          gemv_variant_t variant,
          gemv_accumulate_t accum=GEMV_ACCUMULATE,
          gemv_transpose_t transp=GEMV_A_B>
static TRINNITY_INLINE void gemv(const unsigned mat_a_w, const unsigned mat_a_h,
                               const T * __restrict__ a,
                               const KD * __restrict__ b,
                               T *__restrict__ c) {

  switch(variant) {

    case GEMV_SIMPLE: {
      switch (transp) {
        case GEMV_A_B: {
          for (unsigned i = 0; i < mat_a_h; i+=1) // for each row of the matrix
          {
            for(unsigned j = 0; j < mat_a_w; j+=1) { // for each column of the matrix
              C sum = static_cast<C>(0);

              for(unsigned k=0; k < mat_a_h; k+=1) { // for each element in the vector
                sum += (C)a[i*mat_a_w + k] * (C)b[k];
              }

              switch (accum) {
                case GEMV_ACCUMULATE: {
                  c[i] += (T)sum;
                } break;

                case GEMV_NO_ACCUMULATE: {
                  c[i] = (T)sum;
                } break;
              }
            }
          }
        } break;

        case GEMV_AT_B: {
          for (unsigned i = 0; i < mat_a_w; i+=1) // for each column of the matrix
          {
            for(unsigned j = 0; j < mat_a_h; j+=1) { // for each row of the matrix
              C sum = static_cast<C>(0);

              for(unsigned k=0; k < mat_a_w; k+=1) { // for each element in the vector
                sum += (C)a[k*mat_a_w + i] * (C)b[k];
              }

              switch (accum) {
                case GEMV_ACCUMULATE: {
                  c[i] += (T)sum;
                } break;

                case GEMV_NO_ACCUMULATE: {
                  c[i] = (T)sum;
                } break;
              }
            }
          }
        } break;

        default: {
          TRINNITY_ERROR("Not implemented");
        } break;
      }
    } break;

    #if defined(TRINNITY_USE_CBLAS_GEMV) || defined(TRINNITY_USE_MKL_GEMV)
    case GEMV_BLAS: {
      triNNity::dense::cpu::internal::blas_gemv_wrap<transp>(mat_a_w, mat_a_h, a, b, accum, c);
    } break;
    #endif

    default: {
      TRINNITY_ERROR("Not implemented");
    } break;
  }
}

}

}

}

#endif //TRINNITY_DENSE_CPU_GEMV_H
