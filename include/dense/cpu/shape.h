/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_DENSE_CPU_SHAPE_H
#define TRINNITY_DENSE_CPU_SHAPE_H

#include <triNNity/config.h>
#include <triNNity/dense/cpu/config.h>
#include <triNNity/view.h>

#include <triNNity/dense/cpu/primitive.h>

#if !defined(TRINNITY_NO_EXCEPTIONS)
#include <stdexcept>
#endif

#if !defined(TRINNITY_NO_ASSERTS)
#include <cassert>
#endif

#include <cstring> // for memcpy

namespace triNNity {

namespace dense {

namespace cpu {

namespace transform {

template <typename T, unsigned k, unsigned kernels, unsigned channels>
static TRINNITY_INLINE void kernel_oihw_to_hwoi(const T * __restrict__ kernel_oihw, T * __restrict__ kernel_hwoi) {

  auto in = triNNity::internal::memory::View4D<const T, kernels, channels, k, k>(kernel_oihw);
  auto out = triNNity::internal::memory::View4D<T, k, k, kernels, channels>(kernel_hwoi);

  for (unsigned kn_it = 0; kn_it < kernels; kn_it++) {
    for (unsigned c_it = 0; c_it < channels; c_it++) {
      for (unsigned ky = 0; ky < k; ky++) {
        for (unsigned kx = 0; kx < k; kx++) {
          out[ky][kx][kn_it][c_it] = in[kn_it][c_it][ky][kx];
        }
      }
    }
  }
}

template <typename T, unsigned k, unsigned kernels, unsigned channels>
static TRINNITY_INLINE void kernel_oihw_to_ihwo(const T * __restrict__ kernel_oihw, T * __restrict__ kernel_ihwo) {

  auto in = triNNity::internal::memory::View4D<const T, kernels, channels, k, k>(kernel_oihw);
  auto out = triNNity::internal::memory::View4D<T, channels, k, k, kernels>(kernel_ihwo);

  for (unsigned kn_it = 0; kn_it < kernels; kn_it++) {
    for (unsigned c_it = 0; c_it < channels; c_it++) {
      for (unsigned ky = 0; ky < k; ky++) {
        for (unsigned kx = 0; kx < k; kx++) {
          out[c_it][ky][kx][kn_it] = in[kn_it][c_it][ky][kx];
        }
      }
    }
  }
}

template <typename T, unsigned k, unsigned kernels, unsigned channels>
static TRINNITY_INLINE void kernel_oihw_to_hwio(const T * __restrict__ kernel_oihw, T * __restrict__ kernel_hwio) {

  auto in = triNNity::internal::memory::View4D<const T, kernels, channels, k, k>(kernel_oihw);
  auto out = triNNity::internal::memory::View4D<T, k, k, channels, kernels>(kernel_hwio);

  for (unsigned kn_it = 0; kn_it < kernels; kn_it++) {
    for (unsigned c_it = 0; c_it < channels; c_it++) {
      for (unsigned ky = 0; ky < k; ky++) {
        for (unsigned kx = 0; kx < k; kx++) {
          out[ky][kx][c_it][kn_it] = in[kn_it][c_it][ky][kx];
        }
      }
    }
  }
}

template <typename T, unsigned k, unsigned kernels, unsigned channels>
static TRINNITY_INLINE void kernel_oihw_to_ohwi(const T * __restrict__ kernel_oihw, T * __restrict__ kernel_ohwi) {

  auto in = triNNity::internal::memory::View4D<const T, kernels, channels, k, k>(kernel_oihw);
  auto out = triNNity::internal::memory::View4D<T, kernels, k, k, channels>(kernel_ohwi);

  for (unsigned kn_it = 0; kn_it < kernels; kn_it++) {
    for (unsigned c_it = 0; c_it < channels; c_it++) {
      for (unsigned ky = 0; ky < k; ky++) {
        for (unsigned kx = 0; kx < k; kx++) {
          out[kn_it][ky][kx][c_it] = in[kn_it][c_it][ky][kx];
        }
      }
    }
  }
}

template <typename T, unsigned k, unsigned kernels, unsigned channels>
static TRINNITY_INLINE void kernel_oihw_to_ohiw(const T * __restrict__ kernel_oihw, T * __restrict__ kernel_ohiw) {

  auto in = triNNity::internal::memory::View4D<const T, kernels, channels, k, k>(kernel_oihw);
  auto out = triNNity::internal::memory::View4D<T, kernels, k, channels, k>(kernel_ohiw);

  for (unsigned kn_it = 0; kn_it < kernels; kn_it++) {
    for (unsigned c_it = 0; c_it < channels; c_it++) {
      for (unsigned ky = 0; ky < k; ky++) {
        for (unsigned kx = 0; kx < k; kx++) {
          out[kn_it][ky][c_it][kx] = in[kn_it][c_it][ky][kx];
        }
      }
    }
  }
}

template <typename T, unsigned channels, unsigned img_h, unsigned img_w>
static TRINNITY_INLINE void image_chw_to_hwc(const T * __restrict__ image_chw, T * __restrict__ image_hwc) {

  auto in = triNNity::internal::memory::View3D<const T, channels, img_h, img_w>(image_chw);
  auto out = triNNity::internal::memory::View3D<T, img_h, img_w, channels>(image_hwc);

  for (unsigned c_it = 0; c_it < channels; c_it++) {
    for (unsigned h_it = 0; h_it < img_h; h_it++) {
      for (unsigned w_it = 0; w_it < img_w; w_it++) {
        out[h_it][w_it][c_it] = in[c_it][h_it][w_it];
      }
    }
  }
}

template <typename T, unsigned channels, unsigned img_h, unsigned img_w>
static TRINNITY_INLINE void image_hwc_to_chw(const T * __restrict__ image_hwc, T * __restrict__ image_chw) {

  auto in = triNNity::internal::memory::View3D<const T, img_h, img_w, channels>(image_hwc);
  auto out = triNNity::internal::memory::View3D<T, channels, img_h, img_w>(image_chw);

  for (unsigned h_it = 0; h_it < img_h; h_it++) {
    for (unsigned w_it = 0; w_it < img_w; w_it++) {
      for (unsigned c_it = 0; c_it < channels; c_it++) {
        out[c_it][h_it][w_it] = in[h_it][w_it][c_it];
      }
    }
  }
}

template <typename T, unsigned channels, unsigned img_h, unsigned img_w>
static TRINNITY_INLINE void image_hwc_to_hcw(const T * __restrict__ image_hwc, T * __restrict__ image_hcw) {

  auto in = triNNity::internal::memory::View3D<const T, img_h, img_w, channels>(image_hwc);
  auto out = triNNity::internal::memory::View3D<T, img_h, channels, img_w>(image_hcw);

  for (unsigned h_it = 0; h_it < img_h; h_it++) {
    for (unsigned w_it = 0; w_it < img_w; w_it++) {
      for (unsigned c_it = 0; c_it < channels; c_it++) {
        out[h_it][c_it][w_it] = in[h_it][w_it][c_it];
      }
    }
  }
}

template <typename T, unsigned channels, unsigned img_h, unsigned img_w>
static TRINNITY_INLINE void image_chw_to_hcw(const T * __restrict__ image_chw, T * __restrict__ image_hcw) {

  auto in = triNNity::internal::memory::View3D<const T, channels, img_h, img_w>(image_chw);
  auto out = triNNity::internal::memory::View3D<T, img_h, channels, img_w>(image_hcw);

  for (unsigned c_it = 0; c_it < channels; c_it++) {
    for (unsigned h_it = 0; h_it < img_h; h_it++) {
      for (unsigned w_it = 0; w_it < img_w; w_it++) {
        out[h_it][c_it][w_it] = in[c_it][h_it][w_it];
      }
    }
  }
}

template <typename T, unsigned channels, unsigned img_h, unsigned img_w>
static TRINNITY_INLINE void image_hcw_to_chw(const T * __restrict__ image_hcw, T * __restrict__ image_chw) {

  auto in = triNNity::internal::memory::View3D<const T, img_h, channels, img_w>(image_hcw);
  auto out = triNNity::internal::memory::View3D<T, channels, img_h, img_w>(image_chw);

  for (unsigned c_it = 0; c_it < channels; c_it++) {
    for (unsigned h_it = 0; h_it < img_h; h_it++) {
      for (unsigned w_it = 0; w_it < img_w; w_it++) {
         out[c_it][h_it][w_it] = in[h_it][c_it][w_it];
      }
    }
  }
}

template <typename T, unsigned channels, unsigned img_h, unsigned img_w>
static TRINNITY_INLINE void image_hcw_to_hwc(const T * __restrict__ image_hcw, T * __restrict__ image_hwc) {

  auto in = triNNity::internal::memory::View3D<const T, img_h, channels, img_w>(image_hcw);
  auto out = triNNity::internal::memory::View3D<T, img_h, img_w, channels>(image_hwc);

  for (unsigned c_it = 0; c_it < channels; c_it++) {
    for (unsigned h_it = 0; h_it < img_h; h_it++) {
      for (unsigned w_it = 0; w_it < img_w; w_it++) {
         out[h_it][w_it][c_it] = in[h_it][c_it][w_it];
      }
    }
  }
}

template <typename T, unsigned img_w, unsigned img_h, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned patch, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE T* im2row_chw_to_hwckk(const T * __restrict__ img) {

  switch (patch) {

    case PATCH_SCAN: {

      constexpr unsigned real_h = img_h + (bound==BOUND_EXPLICIT ? k-1 : 0);
      constexpr unsigned real_w = img_w + (bound==BOUND_EXPLICIT ? k-1 : 0);
      auto localImageLayout = triNNity::internal::memory::View3D<const T, channels, real_h, real_w>(img);

      constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>();
      constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();

      T* out_data = new T[out_h*out_w*channels*k*k];

      auto localOutputLayout = triNNity::internal::memory::View4D<T, out_h, out_w, channels, k*k>(out_data);
      #if defined(TRINNITY_USE_OPENMP)
      #pragma omp parallel
      #endif
      {

        // Do the non-boundary pixels
        #if defined(TRINNITY_USE_OPENMP)
        #pragma omp for nowait
        #endif
        for (unsigned c_it = 0; c_it < channels; c_it++) {
          for (unsigned y = (k/2); y < (img_h - (k/2)); y+=stride_h) {
            for (unsigned x = (k/2); x < (img_w - (k/2)); x+=stride_w) {
              triNNity::dense::cpu::primitive::makeConvPatch<T, k, k, img_w, img_h, bound>(x, y, localImageLayout[c_it].begin(), localOutputLayout[y/stride_h][x/stride_w][c_it]);
            }
          }
        }

        // Now do the boundary pixels
        // The top rows of the image
        #if defined(TRINNITY_USE_OPENMP)
        #pragma omp for nowait
        #endif
        for (unsigned c_it = 0; c_it < channels; c_it++) {
          for (unsigned y = 0; y < (k/2); y+=stride_h) {
            for (unsigned x = 0; x < img_w; x+=stride_w) {
              triNNity::dense::cpu::primitive::makeConvPatchBoundary<T, k, k, img_w, img_h, bound>(x, y, localImageLayout[c_it].begin(), localOutputLayout[y/stride_h][x/stride_w][c_it]);
            }
          }
        }

        // The left columns of the image
        #if defined(TRINNITY_USE_OPENMP)
        #pragma omp for nowait
        #endif
        for (unsigned c_it = 0; c_it < channels; c_it++) {
          for (unsigned y = 0; y < img_h; y+=stride_h) {
            for (unsigned x = 0; x < (k/2); x+=stride_w) {
              triNNity::dense::cpu::primitive::makeConvPatchBoundary<T, k, k, img_w, img_h, bound>(x, y, localImageLayout[c_it].begin(), localOutputLayout[y/stride_h][x/stride_w][c_it]);
            }
          }
        }

        // The right columns of the image
        #if defined(TRINNITY_USE_OPENMP)
        #pragma omp for nowait
        #endif
        for (unsigned c_it = 0; c_it < channels; c_it++) {
          for (unsigned y = 0; y < img_h; y+=stride_h) {
            for (unsigned x = (img_w) - k/2; x < img_w; x+=stride_w) {
              triNNity::dense::cpu::primitive::makeConvPatchBoundary<T, k, k, img_w, img_h, bound>(x, y, localImageLayout[c_it].begin(), localOutputLayout[y/stride_h][x/stride_w][c_it]);
            }
          }
        }

        // The bottom rows of the image
        #if defined(TRINNITY_USE_OPENMP)
        #pragma omp for nowait
        #endif
        for (unsigned c_it = 0; c_it < channels; c_it++) {
          for (unsigned y = (img_h) - k/2; y < img_h; y+=stride_h) {
            for (unsigned x = 0; x < img_w; x+=stride_w) {
              triNNity::dense::cpu::primitive::makeConvPatchBoundary<T, k, k, img_w, img_h, bound>(x, y, localImageLayout[c_it].begin(), localOutputLayout[y/stride_h][x/stride_w][c_it]);
            }
          }
        }

      }//OMP parallel scope

      return out_data;

    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    }

  }
}

template <typename T, unsigned img_w, unsigned img_h, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned patch, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE T* im2row_hwc_to_hwkkc(const T * __restrict__ img) {

  switch (patch) {

    case PATCH_COPY_SHORT: {

      switch (bound) {

        case BOUND_UNDEF: // Any mode is acceptable with BOUND_UNDEF, so we should fall through to the fastest legal mode

        case BOUND_IMPLICIT_PAD: {
          constexpr unsigned hstrided = triNNity::uceil<img_h, stride_h>();
          constexpr unsigned wstrided = triNNity::uceil<img_w, stride_w>();
          constexpr unsigned krad = (k/2);
          T* out_data = new T[hstrided*wstrided*k*k*channels];
          #if defined(TRINNITY_USE_OPENMP)
          #pragma omp parallel for
          #endif
          for (unsigned h = 0; h < img_h; h+=stride_h) {
            unsigned lb = h < krad ? krad - h : 0; //how many leading blocks of zeroes this section
            unsigned tb = (img_h-h) < (krad+1) ? (krad+1) - (img_h-h) : 0; //how many trailing blocks
            unsigned hrow = h > krad ? h-krad : 0; //what row is top-left of kernel at? (0 if still in padding)
            for (unsigned w = 0; w < img_w; w+=stride_w) {
              unsigned hwcounter = (h/stride_h)*wstrided + (w/stride_w);
              T * matrow = &(out_data[hwcounter * k*k*channels]);
              unsigned ls = w < krad ? krad - w : 0; //how many leading zeroes per block
              unsigned ts = (img_w-w) < (krad+1) ? (krad+1) - (img_w-w) : 0; //how many trailing zeroes
              unsigned imgIndent = w > krad ? w - krad : 0; //what column is top-left of kernel at?
              for (unsigned i = 0; i < lb*k*channels; i++) { //fill in leading block of zeroes on this row
                matrow[i] = 0;
              }
              for (unsigned krow = lb; krow < k-tb; krow++) { //the non-zero blocks
                for (unsigned i = krow*k*channels; i < krow*k*channels + ls*channels; i++) { //leading zeroes
                  matrow[i] = 0;
                }
                unsigned copyLen = (k*channels) - (ls*channels + ts*channels);
                unsigned imgPos = hrow*img_w*channels + (krow-lb)*img_w*channels + imgIndent*channels;
                memcpy(&matrow[krow*k*channels + ls*channels], &img[imgPos], sizeof(T)*copyLen);
                for (unsigned i = (krow+1)*k*channels - ts*channels; i < (krow+1)*k*channels; i++) { //trailing zeroes
                  matrow[i] = 0;
                }
              }
              for (unsigned i = (k-tb)*k*channels; i <k*k*channels; i++ ) { //fill in the trailing block of zeroes
                matrow[i] = 0;
              }
            }
          }

          return out_data;

        } break;

        case BOUND_EXPLICIT: {
          constexpr unsigned kdia = k-1;
          constexpr unsigned real_h = img_h+kdia;
          constexpr unsigned real_w = img_w+kdia;
          constexpr unsigned hstrided = triNNity::uceil<img_h, stride_h>();
          constexpr unsigned wstrided = triNNity::uceil<img_w, stride_w>();
          T* out_data = new T[hstrided*wstrided*k*k*channels];
          auto image = triNNity::internal::memory::View3D<const T, real_h, real_w, channels>(img);
          auto output = triNNity::internal::memory::View4D<T, hstrided, wstrided, k, k*channels>(out_data);
          for (unsigned h = 0; h < hstrided; h++) {
            for (unsigned w = 0; w < wstrided; w++) {
              for (unsigned ki = 0; ki < k; ki++) {
                unsigned start_h = h*stride_h + ki;
                unsigned start_w = w*stride_w;
                memcpy(&output[h][w][ki][0], &image[start_h][start_w][0], k*channels*sizeof(T));
              }
            }
          }
          return out_data;
        } break;

        default: {
          TRINNITY_ERROR("Not implemented");
        }
      }

    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    }
  }
}

template <typename T, unsigned img_w, unsigned img_h, unsigned channels, unsigned kernels, unsigned k, boundary_t bound, unsigned patch, unsigned stride_w, unsigned stride_h>
static TRINNITY_INLINE T* im2col_chw_to_ckkhw(const T * __restrict__ img) {
  switch (patch) {

    case PATCH_SCAN: {
      constexpr unsigned real_h = img_h + (bound==BOUND_EXPLICIT ? k-1 : 0);
      constexpr unsigned real_w = img_w + (bound==BOUND_EXPLICIT ? k-1 : 0);
      auto localImageLayout = triNNity::internal::memory::View3D<const T, channels, real_h, real_w>(img);

      constexpr unsigned out_w = triNNity::uceil<img_w, stride_w>();
      constexpr unsigned out_h = triNNity::uceil<img_h, stride_h>();

      T* out_data = new T[channels*k*k*out_h*out_w];

      auto localOutputLayout = triNNity::internal::memory::View5D<T, channels, k, k, out_h, out_w>(out_data);
      #if defined(TRINNITY_USE_OPENMP)
      #pragma omp parallel
      #endif
      {

        // Do the non-boundary pixels
        #if defined(TRINNITY_USE_OPENMP)
        #pragma omp for nowait
        #endif
        for (unsigned c_it = 0; c_it < channels; c_it++) {
          for (unsigned y = (k/2); y < (img_h - (k/2)); y+=stride_h) {
            for (unsigned x = (k/2); x < (img_w - (k/2)); x+=stride_w) {
              T* buf = new T[k*k];
              triNNity::dense::cpu::primitive::makeConvPatch<T, k, k, img_w, img_h, bound>(x, y, localImageLayout[c_it].begin(), buf);

              for (unsigned ky = 0; ky < k; ky++) {
                for (unsigned kx = 0; kx < k; kx++) {
                  localOutputLayout[c_it][ky][kx][y/stride_h][x/stride_w] = buf[ky*k + kx];
                }
              }

              delete [] buf;
            }
          }
        }

        // Now do the boundary pixels
        // The top rows of the image
        #if defined(TRINNITY_USE_OPENMP)
        #pragma omp for nowait
        #endif
        for (unsigned c_it = 0; c_it < channels; c_it++) {
          for (unsigned y = 0; y < (k/2); y+=stride_h) {
            for (unsigned x = 0; x < img_w; x+=stride_w) {
              T* buf = new T[k*k];
              triNNity::dense::cpu::primitive::makeConvPatchBoundary<T, k, k, img_w, img_h, bound>(x, y, localImageLayout[c_it].begin(), buf);

              for (unsigned ky = 0; ky < k; ky++) {
                for (unsigned kx = 0; kx < k; kx++) {
                  localOutputLayout[c_it][ky][kx][y/stride_h][x/stride_w] = buf[ky*k + kx];
                }
              }

              delete [] buf;
            }
          }
        }

        // The left columns of the image
        #if defined(TRINNITY_USE_OPENMP)
        #pragma omp for nowait
        #endif
        for (unsigned c_it = 0; c_it < channels; c_it++) {
          for (unsigned y = 0; y < img_h; y+=stride_h) {
            for (unsigned x = 0; x < (k/2); x+=stride_w) {
              T* buf = new T[k*k];
              triNNity::dense::cpu::primitive::makeConvPatchBoundary<T, k, k, img_w, img_h, bound>(x, y, localImageLayout[c_it].begin(), buf);

              for (unsigned ky = 0; ky < k; ky++) {
                for (unsigned kx = 0; kx < k; kx++) {
                  localOutputLayout[c_it][ky][kx][y/stride_h][x/stride_w] = buf[ky*k + kx];
                }
              }

              delete [] buf;
            }
          }
        }

        // The right columns of the image
        #if defined(TRINNITY_USE_OPENMP)
        #pragma omp for nowait
        #endif
        for (unsigned c_it = 0; c_it < channels; c_it++) {
          for (unsigned y = 0; y < img_h; y+=stride_h) {
            for (unsigned x = (img_w) - k/2; x < img_w; x+=stride_w) {
              T* buf = new T[k*k];
              triNNity::dense::cpu::primitive::makeConvPatchBoundary<T, k, k, img_w, img_h, bound>(x, y, localImageLayout[c_it].begin(), buf);

              for (unsigned ky = 0; ky < k; ky++) {
                for (unsigned kx = 0; kx < k; kx++) {
                  localOutputLayout[c_it][ky][kx][y/stride_h][x/stride_w] = buf[ky*k + kx];
                }
              }

              delete [] buf;
            }
          }
        }

        // The bottom rows of the image
        #if defined(TRINNITY_USE_OPENMP)
        #pragma omp for nowait
        #endif
        for (unsigned c_it = 0; c_it < channels; c_it++) {
          for (unsigned y = (img_h) - k/2; y < img_h; y+=stride_h) {
            for (unsigned x = 0; x < img_w; x+=stride_w) {
              T* buf = new T[k*k];
              triNNity::dense::cpu::primitive::makeConvPatchBoundary<T, k, k, img_w, img_h, bound>(x, y, localImageLayout[c_it].begin(), buf);

              for (unsigned ky = 0; ky < k; ky++) {
                for (unsigned kx = 0; kx < k; kx++) {
                  localOutputLayout[c_it][ky][kx][y/stride_h][x/stride_w] = buf[ky*k + kx];
                }
              }

              delete [] buf;
            }
          }
        }

      }//OMP parallel scope

      return out_data;

    } break;

    case PATCH_COPY_SHORT: {

      TRINNITY_ASSERT_ON(patch==PATCH_COPY_SHORT, stride_w == 1);
      TRINNITY_ASSERT_ON(patch==PATCH_COPY_SHORT, stride_h == 1);
      switch (bound) {

        case BOUND_UNDEF:
        case BOUND_IMPLICIT_PAD: {
          auto image = triNNity::internal::memory::View2D<const T, channels, img_h*img_w>(img);
          T* out_data = new T[channels*k*k*img_h*img_w];
          auto output = triNNity::internal::memory::View3D<T, channels, k*k, img_h*img_w>(out_data);
          constexpr unsigned krad = (k/2);
          #if defined(TRINNITY_USE_OPENMP)
          #pragma omp parallel for
          #endif
          for (unsigned c_it = 0; c_it < channels; c_it++) {
            unsigned kkcounter = 0;

            //leading block section
            for (signed lb = krad; lb >= 0; lb--) {
              for (unsigned ls = krad; ls > 0; ls--) { //leading zeroes
                for (unsigned i = 0; i < lb*img_w; i++) { //filling in the leading zeroes
                  output[c_it][kkcounter][i] = 0;
                }
                unsigned imgPos = 0;
                unsigned hwcounter = lb * img_w;
                for (unsigned j = lb; j < img_h; j++) { //non-zero blocks
                  for (unsigned m = 0; m < ls; m++) { //filling in the LS
                    output[c_it][kkcounter][hwcounter] = 0;
                    hwcounter++;
                  }
                  memcpy(&output[c_it][kkcounter][hwcounter], &image[c_it][imgPos], sizeof(T)*(img_w-ls));
                  imgPos += img_w;
                  hwcounter += img_w - ls;
                }
                kkcounter++;
              }
              //no LS or TS line
              for (unsigned i = 0; i < lb * img_w; i++) {
                output[c_it][kkcounter][i] = 0;
              }
              unsigned copyLen = (img_h*img_w) - (lb*img_w);
              memcpy(&output[c_it][kkcounter][lb*img_w], &image[c_it][0], sizeof(T)*copyLen);
              kkcounter++;
              for (unsigned ts = 1; ts <= krad; ts++) { //trailing zeroes
                for (unsigned i = 0; i < lb*img_w; i++) { //filling in the LB
                  output[c_it][kkcounter][i] = 0;
                }
                unsigned imgPos = ts;
                unsigned hwcounter = lb * img_w;
                for (unsigned j = lb; j < img_h; j++) { //non-zero blocks
                  memcpy(&output[c_it][kkcounter][hwcounter], &image[c_it][imgPos], sizeof(T)*(img_w-ts));
                  imgPos += img_w;
                  hwcounter += img_w - ts;
                  for (unsigned m = 0; m < ts; m++) { //filling in the TS
                    output[c_it][kkcounter][hwcounter] = 0;
                    hwcounter++;
                  }
                }
                kkcounter++;
              }
            }

            //trailing block section
            for (unsigned tb = 1; tb <= krad; tb++) {
              for (signed ls = krad; ls > 0; ls--) { //leading singles
                unsigned imgPos = tb*img_w;
                unsigned hwcounter = 0;
                for (unsigned j = 0; j < img_h-tb; j++) { //non-zero blocks
                  for (signed m = 0; m < ls; m++) { //filling in the LS
                    output[c_it][kkcounter][hwcounter] = 0;
                    hwcounter++;
                  }
                  memcpy(&output[c_it][kkcounter][hwcounter], &image[c_it][imgPos], sizeof(T)*(img_w-ls));
                  imgPos += img_w;
                  hwcounter += img_w - ls;
                }
                for (unsigned i = (img_h-tb)*img_w; i < img_h*img_w; i++) {
                  output[c_it][kkcounter][i] = 0;
                }
                kkcounter++;
              }
              //no LS or TS line
              unsigned copyLen = (img_h*img_w) - (tb*img_w);
              memcpy(&output[c_it][kkcounter][0], &image[c_it][tb*img_w], sizeof(T)*copyLen);
              for (unsigned i = (img_h-tb)*img_w; i < img_h*img_w; i++) { //filling in the TB
                output[c_it][kkcounter][i] = 0;
              }
              kkcounter++;
              for (unsigned ts = 1; ts <= krad; ts++) { //trailing singles
                unsigned imgPos = tb*img_w + ts;
                unsigned hwcounter = 0;
                for (unsigned j = 0; j < img_h-tb; j++) { //non-zero blocks
                  memcpy(&output[c_it][kkcounter][hwcounter], &image[c_it][imgPos], sizeof(T)*(img_w-ts));
                  imgPos += img_w;
                  hwcounter += img_w - ts;
                  for (unsigned m = 0; m < ts; m++) { //filling in the TS
                    output[c_it][kkcounter][hwcounter] = 0;
                    hwcounter++;
                  }
                }
                for (unsigned i = (img_h-tb)*img_w; i < img_h*img_w; i++) { //filling in the TB
                  output[c_it][kkcounter][i] = 0;
                }
                kkcounter++;
              }
            }
          }

          return out_data;

        } break;

        case BOUND_EXPLICIT: {
          constexpr unsigned kdia = k-1;
          constexpr unsigned real_h = img_h+kdia;
          constexpr unsigned real_w = img_w+kdia;
          auto image = triNNity::internal::memory::View3D<const T, channels, real_h, real_w>(img);
          T* out_data = new T[channels*k*k*img_h*img_w];
          auto output = triNNity::internal::memory::View5D<T, channels, k, k, img_h, img_w>(out_data);
          for (unsigned c = 0; c < channels; c++) {
            for (unsigned ky = 0; ky < k; ky++) {
              for (unsigned kx = 0; kx < k; kx++) {
                for (unsigned h = 0; h < img_h; h++) {
                  memcpy(&output[c][ky][kx][h][0], &image[c][h+ky][kx], img_w*sizeof(T));
                }
              }
            }
          }
          return out_data;
        } break;

        default: {
          TRINNITY_ERROR("Not implemented");
        }
      }

    } break;

    case PATCH_COPY_LONG: {

      TRINNITY_ASSERT_ON(patch==PATCH_COPY_LONG, stride_w == 1);
      TRINNITY_ASSERT_ON(patch==PATCH_COPY_LONG, stride_h == 1);
      switch (bound) {

        case BOUND_UNDEF:
        case BOUND_IMPLICIT_PAD: {
          auto image = triNNity::internal::memory::View2D<const T, channels, img_h*img_w>(img);
          T* out_data = new T[channels*k*k*img_h*img_w];
          auto output = triNNity::internal::memory::View3D<T, channels, k*k, img_h*img_w>(out_data);
          constexpr unsigned krad = (k/2);
          #if defined(TRINNITY_USE_OPENMP)
          #pragma omp parallel for
          #endif
          for (unsigned c_it = 0; c_it < channels; c_it++) {
            unsigned kkcounter = 0;

            //leading block section
            for (signed lb = krad; lb >= 0; lb--) {
              for (signed ls = krad; ls >= 0; ls--) { //leading zeroes
                for (unsigned i = 0; i < img_w*lb; i++) { //filling in the LB
                  output[c_it][kkcounter][i] = 0;
                }
                unsigned copyLen = (img_h*img_w) - ((lb*img_w) + ls);
                memcpy(&output[c_it][kkcounter][lb*img_w + ls], &image[c_it][0], sizeof(T)*copyLen);
                for (unsigned j = lb; j < img_h; j++) { //the non-zero blocks
                  unsigned blockStartCol = j*img_w;
                  for (signed m = 0; m < ls; m++) { //filling in the LS
                    output[c_it][kkcounter][blockStartCol+m] = 0;
                  }
                }
                kkcounter++;
              }
              for (unsigned ts = 1; ts <= krad; ts++) { //trailing zeroes
                for (unsigned i = 0; i < img_w; i++) { //filling in the LB
                  output[c_it][kkcounter][i] = 0;
                }
                unsigned copyLen = (img_h*img_w) - (lb*img_w + ts);
                memcpy(&output[c_it][kkcounter][lb*img_w], &image[c_it][ts], sizeof(T)*copyLen);
                for (unsigned j = lb+1; j <img_h+1; j++) { //the non-zero blocks
                  unsigned blockEndCol = (j*img_w) - 1;
                  for (unsigned m = 0; m < ts; m++) { //filling in the TS
                    output[c_it][kkcounter][blockEndCol-m] = 0;
                  }
                }
                kkcounter++;
              }
            }

            //trailing block section
            for (unsigned tb = 1; tb <= krad; tb++) {
              unsigned imgPos = tb*img_w;
              for (signed ls = krad; ls >= 0; ls--) { //leading singles
                unsigned copyLen = (img_h*img_w) - ((tb*img_w)+ls);
                memcpy(&output[c_it][kkcounter][ls], &image[c_it][imgPos], sizeof(T)*copyLen);
                for (unsigned j = 0; j < img_h-tb; j++) { //the non-zero blocks
                  unsigned blockStartCol = j*img_w;
                  for (signed m = 0; m < ls; m++) { //filling in the LS
                    output[c_it][kkcounter][blockStartCol+m] = 0;
                  }
                }
                for (unsigned i = (img_h-tb)*img_w; i < img_h*img_w; i++) { //filling in the TB
                  output[c_it][kkcounter][i] = 0;
                }
                kkcounter++;
              }
              for (unsigned ts = 1; ts <= krad; ts++) { //trailing singles
                imgPos++;
                unsigned copyLen = (img_h*img_w) - (img_w*tb + ts);
                memcpy(&output[c_it][kkcounter][0], &image[c_it][imgPos], sizeof(T)*copyLen);
                for (unsigned j = 1; j < (img_h-tb)+1; j++) { //the non-zero blocks
                  unsigned blockEndCol = (j*img_w) - 1;
                  for (unsigned m = 0; m < ts; m++) { //filling in the TS
                    output[c_it][kkcounter][blockEndCol-m] = 0;
                  }
                }
                for (unsigned i = (img_h-tb)*img_w; i < img_h*img_w; i++) { //filling in the TB
                  output[c_it][kkcounter][i] = 0;
                }
                kkcounter++;
              }
            }
          }

          return out_data;

        } break;

        default: {
          TRINNITY_ERROR("Not implemented");
        }
      }

    } break;

    case PATCH_COPY_SELF: {

      switch (bound) {

        case BOUND_UNDEF:
        case BOUND_IMPLICIT_PAD: {

          constexpr unsigned wstrided = triNNity::uceil<img_w, stride_w>();
          constexpr unsigned hstrided = triNNity::uceil<img_h, stride_h>();
          constexpr unsigned lastwoffset = (img_w-1) - (stride_w*((img_w-1)/stride_w));
          constexpr unsigned lasthoffset = (img_h-1) - (stride_h*((img_h-1)/stride_h));
          constexpr unsigned krad = k/2;
          constexpr unsigned rowlength = hstrided*wstrided;
          T * out_data = new T[channels*k*k*hstrided*wstrided];
          #if defined(TRINNITY_USE_OPENMP)
          #pragma omp parallel for
          #endif
          for (unsigned c = 0; c < channels; c++) {
            T * mat = &(out_data[c*k*k*hstrided*wstrided]);
            const T * imgl = &(img[c*img_h*img_h]);
            unsigned kkcounter = 0;
            //gathering sections, ones that can't memcpy entire blocks from previous sections
            for (unsigned gs = 0; (gs < stride_h && gs < k); gs++) {
              unsigned lb = krad > gs ? ((krad-gs)/stride_h) + (((krad-gs)%stride_h)>0) : 0;
              unsigned revgs = k-(gs+1);
              unsigned tb = krad > (revgs+lasthoffset) ? ((krad-(revgs+lasthoffset))/stride_h) + (((krad - (revgs+lasthoffset))%stride_h)>0) : 0;
              //lines that have to gather the values from the image and memcopy it to the other lines in the section
              for (unsigned gl = 0; (gl < stride_w) && (gl < k); gl++) {
                unsigned ls = krad > gl ? ((krad-gl)/stride_w) + (((krad-gl)%stride_w)>0) : 0;
                unsigned revgl = k-(gl+1);
                unsigned ts = krad > (revgl+lastwoffset) ? ((krad-(revgl+lastwoffset))/stride_w) + (((krad - (revgl+lastwoffset))%stride_w)>0) : 0;
                for (unsigned i = 0; i < lb*wstrided + ls; i++) { mat[kkcounter*rowlength + i] = 0; }
                if (lb > 0) { //matters if we have leading block of zeroes as can overead on the first line and the overflow values will go in the leading zero part of the next line
                  //gathering the needed values from image
                  unsigned hwcounter = lb*wstrided + ls;
                  for (unsigned bw = ls; bw < wstrided+ls; bw++) {
                    mat[kkcounter*rowlength + hwcounter] = imgl[(lb*stride_h + gs-krad)*img_w + (bw*stride_w + gl-krad)];
                    hwcounter++;
                  }
                  for (unsigned bh = lb+1; bh < hstrided - tb; bh++) {
                    for (unsigned bw = ls; bw < wstrided+ls; bw++) {
                      mat[kkcounter*rowlength + hwcounter] = imgl[(bh*stride_h + gs-krad)*img_w + (bw*stride_w + gl-krad)];
                      hwcounter++;
                    }
                  }
                  if (hstrided-lb > 0) {
                    //memcopying the values to later lines in this section
                    unsigned fljumps = 1;
                    for (unsigned fl = gl+stride_w; fl < k; fl+=stride_w) {
                      unsigned destls = krad > fl ? ((krad-fl)/stride_w) + (((krad-fl)%stride_w)>0) : 0;
                      unsigned destrevgl = k-(fl+1);
                      unsigned destts = krad > (destrevgl+lastwoffset) ? ((krad-(destrevgl+lastwoffset))/stride_w) + (((krad - (destrevgl+lastwoffset))%stride_w)>0) : 0;
                      unsigned destoffset = destls;
                      unsigned srcoffset = fljumps > ls ? fljumps-ls : 0;
                      memcpy(&mat[(fl + gs*k)*rowlength + lb*wstrided+destoffset], &mat[kkcounter*rowlength + lb*wstrided+ls+srcoffset], sizeof(T)*(hstrided-lb)*wstrided - sizeof(T)*(destls+destts));
                      fljumps++;
                    }
                  }

                } else { //no leading zero block
                  //gathering the needed values from image
                  unsigned hwcounter = ls;
                  for (unsigned bw = ls; bw < wstrided+ls; bw++) {
                    mat[kkcounter*rowlength + hwcounter] = imgl[(lb*stride_h + gs-krad)*img_w + (bw*stride_w + gl-krad)];
                    hwcounter++;
                  }
                  for (unsigned bh = 1; bh < hstrided - (tb+1); bh++) {
                    for (unsigned bw = ls; bw < wstrided+ls; bw++) {
                      mat[kkcounter*rowlength + hwcounter] = imgl[(bh*stride_h + gs-krad)*img_w + (bw*stride_w + gl-krad)];
                      hwcounter++;
                    }
                  }
                  for (unsigned bw = ls; bw < wstrided; bw++) {
                    mat[kkcounter*rowlength + hwcounter] = imgl[((hstrided-(tb+1))*stride_h + gs-krad)*img_w + (bw*stride_w + gl-krad)];
                    hwcounter++;
                  }
                  //memcopying the values to later lines in this section
                  unsigned fljumps = 1;
                  for (unsigned fl = gl+stride_w; fl < k; fl+=stride_w) {
                    unsigned destls = krad > fl ? ((krad-fl)/stride_w) + (((krad-fl)%stride_w)>0) : 0;
                    unsigned destrevgl = k-(fl+1);
                    unsigned destts = krad > (destrevgl+lastwoffset) ? ((krad-(destrevgl+lastwoffset))/stride_w) + (((krad - (destrevgl+lastwoffset))%stride_w)>0) : 0;
                    unsigned destoffset = destls;
                    unsigned srcoffset = fljumps > ls ? fljumps-ls : 0;
                    memcpy(&mat[(fl + gs*k)*rowlength + lb*wstrided+destoffset], &mat[kkcounter*rowlength + lb*wstrided+ls+srcoffset], sizeof(T)*(hstrided-lb)*wstrided - sizeof(T)*(destls+destts+srcoffset));
                    fljumps++;
                  }
                }

                for (unsigned b = lb; b < hstrided; b++) {
                  unsigned blockstart = b*wstrided;
                  unsigned blockend = (b+1)*wstrided-1;
                  for (unsigned i = 0; i < ls; i++) {
                    mat[kkcounter*rowlength +blockstart+i] = 0;
                  }
                  for (unsigned i = 0; i < ts; i++) {
                    mat[kkcounter*rowlength +blockend-i] = 0;
                  }
                }


                for (unsigned i = (hstrided-tb)*wstrided; i < hstrided*wstrided; i++) { mat[kkcounter*rowlength + i] = 0; }

                kkcounter++;
              }

              for (unsigned cl = stride_w; cl < k; cl++) {
                unsigned ls = krad > cl ? ((krad-cl)/stride_w) + (((krad-cl)%stride_w)>0) : 0;
                unsigned revcl = k-(cl+1);
                unsigned ts = krad > (revcl+lastwoffset) ? ((krad-(revcl+lastwoffset))/stride_w) + (((krad - (revcl+lastwoffset))%stride_w)>0) : 0;
                for (unsigned i = 0; i < lb*wstrided + ls; i++) { mat[kkcounter*rowlength + i] = 0; }

                if (lb == 0) {
                  unsigned h = (hstrided - tb)-1;
                  for (unsigned w = 0; w < wstrided-ts; w++) {
                    mat[kkcounter*rowlength + (h*wstrided+w)] = imgl[(h*stride_h + gs-krad)*img_w + (w*stride_w + cl-krad)];
                  }
                }

                for (unsigned b = lb; b < hstrided; b++) {
                  unsigned blockstart = b*wstrided;
                  unsigned blockend = (b+1)*wstrided-1;
                  for (unsigned i = 0; i < ls; i++) {
                    mat[kkcounter*rowlength +blockstart+i] = 0;
                  }
                  for (unsigned i = 0; i < ts; i++) {
                    mat[kkcounter*rowlength +blockend-i] = 0;
                  }
                }

                for (unsigned i = (hstrided-tb)*wstrided; i < hstrided*wstrided; i++) { mat[kkcounter*rowlength + i] = 0; }

                kkcounter++;
              }

            }
            //copying section
            for (unsigned cs = stride_h; cs < k; cs++) {
              unsigned revcs = k-(cs+1);
              unsigned tb = krad > (revcs+lasthoffset) ? ((krad-(revcs+lasthoffset))/stride_h) + (((krad - (revcs+lasthoffset))%stride_h)>0) : 0;
              for (unsigned cl = 0; cl < k; cl++) {
                unsigned ls = krad > cl ? ((krad-cl)/stride_w) + (((krad-cl)%stride_w)>0) : 0;
                unsigned revcl = k-(cl+1);
                unsigned ts = krad > (revcl+lastwoffset) ? ((krad-(revcl+lastwoffset))/stride_w) + (((krad - (revcl+lastwoffset))%stride_w)>0) : 0;
                memcpy(&mat[kkcounter*rowlength], &mat[(kkcounter-(stride_h*k))*rowlength + wstrided], sizeof(T)*(hstrided-1)*wstrided);
                if (tb != 0) {
                  for (unsigned i = (hstrided-1)*wstrided; i < hstrided*wstrided; i++) {
                    mat[kkcounter*rowlength + i] = 0;
                  }
                } else {
                  unsigned h = (hstrided)-1;
                  for (unsigned w = 0; w < wstrided-ts; w++) {
                    mat[kkcounter*rowlength + (h*wstrided+w)] = imgl[(h*stride_h + cs-krad)*img_w + (w*stride_w + cl-krad)];
                  }

                  for (unsigned b = 0; b < hstrided; b++) {
                    unsigned blockstart = b*wstrided;
                    unsigned blockend = (b+1)*wstrided-1;
                    for (unsigned i = 0; i < ls; i++) {
                      mat[kkcounter*rowlength +blockstart+i] = 0;
                    }
                    for (unsigned i = 0; i < ts; i++) {
                      mat[kkcounter*rowlength +blockend-i] = 0;
                    }
                  }
                }
                kkcounter++;
              }
            }
          }
          return out_data;

        } break;

        default: {
          TRINNITY_ERROR("Not implemented");
        }
      }

    } break;

    default: {
      TRINNITY_ERROR("Not implemented");
    }
  }
}

}

}

}

}

#endif // TRINNITY_DENSE_CPU_SHAPE_H
