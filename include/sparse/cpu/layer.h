/*
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
*/
#ifndef TRINNITY_SPARSE_CPU_LAYER_H
#define TRINNITY_SPARSE_CPU_LAYER_H

#include <cstddef>

#include <triNNity/config.h>
#include <triNNity/layer.h>

#include <triNNity/dense/cpu/config.h>
#include <triNNity/dense/cpu/gemv.h>
#include <triNNity/dense/cpu/mcmk.h>
#include <triNNity/dense/cpu/shape.h>
#include <triNNity/dense/cpu/memory.h>
#include <triNNity/dense/cpu/primitive.h>

#include <triNNity/sparse/cpu/config.h>
#include <triNNity/sparse/cpu/primitive.h>
#include <triNNity/sparse/cpu/impl.h>

namespace triNNity {

namespace layer {

/**
 * A layer which performs direct sparse convolution.
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor of the layer
 * \endparblock
 * \tparam sparse_method
 * \parblock
 * The sparse convolution method the layer should use to compute the output
 * \endparblock
 * \tparam image_layout
 * \parblock
 * The layout that the input image is in
 * \endparblock
 * \tparam kernel_layout
 * \parblock
 * The layout that the kernel is in
 * \endparblock
 * \tparam output_layout
 * \parblock
 * The layout that the output should be in
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam k
 * \parblock
 * The radix k of the convolution
 * \endparblock
 * \tparam stride_w
 * \parblock
 * The stride in the width dimension of the convolution
 * \endparblock
 * \tparam stride_h
 * \parblock
 * The stride in the height dimension of the convolution
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 * \tparam ofm_w
 * \parblock
 * The width of output feature maps
 * \endparblock
 * \tparam ofm_h
 * \parblock
 * The height of output feature maps
 * \endparblock
 * \tparam bound_type
 * \parblock
 * The type of boundary handling that the layer should use
 * \endparblock
 *
 */

template<typename T, typename KD,
         triNNity::conv_sparse_impl_t sparse_method,
         triNNity::layout::feature_map_layout_t image_layout,
         triNNity::layout::parameter_layout_t kernel_layout,
         triNNity::layout::feature_map_layout_t output_layout,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned k,
         unsigned stride_w, unsigned stride_h,
         unsigned ofms, unsigned ofm_w, unsigned ofm_h,
         triNNity::boundary_t bound_type = BOUND_IMPLICIT_PAD>
struct DirectSparseConvolutionalLayer: public Layer<T, T> {

  //! Pointer to the sparse kernel data in memory
  const triNNity::sparse::cpu::primitive::sparse_kernel<KD> *kernel;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    // Run the convolution
    switch (sparse_method) {
      case CONV_MULTI_SPARSE_DIRECT_MHW: {
        triNNity::sparse::cpu::impl::sparse_loop_mhw<T, KD, ifm_w, ifm_h, ifms, ofms, k, bound_type, stride_w, stride_h>(this->input, this->kernel, this->output);
      } break;
      case CONV_MULTI_SPARSE_DIRECT_MWH: {
        triNNity::sparse::cpu::impl::sparse_loop_mwh<T, KD, ifm_w, ifm_h, ifms, ofms, k, bound_type, stride_w, stride_h>(this->input, this->kernel, this->output);
      } break;
      case CONV_MULTI_SPARSE_DIRECT_MHW_UNROLL_W: {
        triNNity::sparse::cpu::impl::sparse_loop_mhw_unroll_w<T, KD, ifm_w, ifm_h, ifms, ofms, k, bound_type, stride_w, stride_h>(this->input, this->kernel, this->output);
      } break;
      case CONV_MULTI_SPARSE_DIRECT_MHW_UNROLL_M: {
        triNNity::sparse::cpu::impl::sparse_loop_mhw_unroll_m<T, KD, ifm_w, ifm_h, ifms, ofms, k, bound_type, stride_w, stride_h>(this->input, this->kernel, this->output);
      } break;
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() {
    double conv_flops = static_cast<double>(2*triNNity::uceil<ifm_h, stride_h>()*triNNity::uceil<ifm_w, stride_w>()*ifms*ofms*k*k);
    return conv_flops;
  }

  virtual double working_space() { return 0; }

  DirectSparseConvolutionalLayer(T *in, KD *weights) {
    TRINNITY_STATIC_ASSERT(kernel_layout == triNNity::layout::OIHW);
    TRINNITY_STATIC_ASSERT(image_layout == triNNity::layout::CHW);
    TRINNITY_STATIC_ASSERT(output_layout == triNNity::layout::CHW);
    TRINNITY_STATIC_ASSERT(bound_type == triNNity::BOUND_IMPLICIT_PAD);

    TRINNITY_STATIC_ASSERT(ofms > 0);

    this->input = in;
    this->output = new T[ofms*ofm_h*ofm_w]();
    this->kernel = triNNity::sparse::cpu::primitive::create_packed_oihw<KD, ifms, ofms, k>(weights);
    this->output_inplace = false;
  }

  DirectSparseConvolutionalLayer(T *in, KD *weights, T *out) {
    TRINNITY_STATIC_ASSERT(kernel_layout == triNNity::layout::OIHW);
    TRINNITY_STATIC_ASSERT(image_layout == triNNity::layout::CHW);
    TRINNITY_STATIC_ASSERT(output_layout == triNNity::layout::CHW);
    TRINNITY_STATIC_ASSERT(bound_type == triNNity::BOUND_IMPLICIT_PAD);

    TRINNITY_STATIC_ASSERT(ofms > 0);

    this->input = in;
    if (out == nullptr) {
      this->output = new T[ofms*ofm_h*ofm_w]();
      this->output_inplace = false;
    } else {
      this->output = out;
      this->output_inplace = true;
    }
    this->kernel = triNNity::sparse::cpu::primitive::create_packed_oihw<KD, ifms, ofms, k>(weights);
  }

  ~DirectSparseConvolutionalLayer() {
    triNNity::sparse::cpu::primitive::destroy_packed_oihw(this->kernel);

    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

/**
 * A layer which performs Sparse FC (fully connected layer).
 *
 * \tparam T
 * \parblock
 * The primitive type of the data in the input and output tensors of the layer
 * \endparblock
 * \tparam KD
 * \parblock
 * The primitive type of the data in the parameter tensor of the layer
 * \endparblock
 * \tparam gemv_impl
 * \parblock
 * What implementation of GEMV to use
 * \endparblock
 * \tparam ifms
 * \parblock
 * The number of input feature maps
 * \endparblock
 * \tparam ifm_w
 * \parblock
 * The width of input feature maps
 * \endparblock
 * \tparam ifm_h
 * \parblock
 * The height of input feature maps
 * \endparblock
 * \tparam ofms
 * \parblock
 * The number of output feature maps
 * \endparblock
 *
 */

template<typename T, typename KD,
         triNNity::fc_sparse_impl_t fc_impl,
         unsigned ifms, unsigned ifm_w, unsigned ifm_h,
         unsigned ofms>
struct SparseFCLayer: public Layer<T, T> {

  //! Pointer to the flat kernel data in memory
  KD *kernel;

  virtual void execute() {
    TRINNITY_TIMESTAMP(&this->start);

    switch (fc_impl) {
      case FC_SPARSE_DIRECT: {
        // Sparse FC code here
        TRINNITY_ERROR("SparseFCLayer: not implemented");
      } break;

      case FC_SPARSE_HAMMING: {
        // Sparse FC code here
        TRINNITY_ERROR("SparseFCLayer: not implemented");
      } break;

      default: {
        TRINNITY_ERROR("SparseFCLayer: unknown FC implementation requested");
      } break;
    }

    TRINNITY_TIMESTAMP(&this->end);
  }

  virtual double flops() { return 0; }

  virtual double working_space() { return 0; }

  SparseFCLayer(T *in, KD *weights) {
    this->input = in;
    this->output = new T[ofms]();
    this->output_inplace = false;
    kernel = weights;
  }

  SparseFCLayer(T *in, KD *weights, T *out) {
    this->input = in;
    this->output = out;
    this->output_inplace = true;
    kernel = weights;
  }

  ~SparseFCLayer() {
    if ((this->output != nullptr) && !(this->output_inplace)) {
      TRINNITY_DELETE_ARRAY(this->output);
    }
  }
};

}

}

#endif //TRINNITY_SPARSE_CPU_LAYER_H
